class Micropost < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy

  default_scope -> { order(created_at: :desc) }
  scope :_user_ids, -> (user_ids) { where(user_id: user_ids) }

  mount_uploader :picture, PictureUploader

  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: Settings.micropost.content_maxlength }
  validate  :picture_size

  private

  # Validates the size of an uploaded picture.
  def picture_size
    return if picture.size <= Settings.micropost.picture_size.megabytes

    errors.add(:picture, I18n.t('messages.failed.micropost.size'))
  end
end
