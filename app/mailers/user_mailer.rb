class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: I18n.t('user_mailer.subject_mail')
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: I18n.t('user_mailer.reset_password_mail')
  end
end
