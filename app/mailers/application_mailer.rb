class ApplicationMailer < ActionMailer::Base
  default from: Settings.mailer.default_from_mail
  layout 'mailer'
end
