class MicropostsController < ApplicationController
  before_action :check_login,    only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  before_action :load_micropost, only: :show
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = I18n.t('messages.success.micropost.created')
      redirect_to root_path
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def show
    @comments = @micropost.comments.paginate(page: params[:page], per_page: Settings.micropost.page)
    @comment = @micropost.comments.build
  end

  def destroy
    if @micropost.destroy
      flash[:success] = I18n.t('messages.success.micropost.delete')
    else
      flash[:danger] = I18n.t('messages.failed.micropost.delete')
    end
    redirect_to request.referrer || root_path
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content, :picture)
  end

  def correct_user
    return if @micropost = current_user.microposts.find_by(id: params[:id])

    flash[:danger] = I18n.t('messages.failed.micropost.not_find')
    redirect_to root_path
  end

  def load_micropost
    return if @micropost = Micropost.find_by(id: params[:id])

    flash[:danger] = I18n.t('messages.failed.micropost.not_find')
    redirect_to root_path
  end
end
