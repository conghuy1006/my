class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  def check_login
    return if logged_in?

    store_location
    flash[:danger] = I18n.t('messages.failed.user.check_login')
    redirect_to login_path
  end

  def logged_in_user
    return unless logged_in?

    flash[:danger] = I18n.t('messages.failed.session.logged_in')
    redirect_to root_path
  end
end
