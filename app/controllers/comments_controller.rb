class CommentsController < ApplicationController
  before_action :check_login
  before_action :comment_owner, only: [:destroy, :edit, :update]
  before_action :load_comment, only: [:edit, :update]

  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash[:success] = I18n.t('messages.success.comment.created')
      redirect_to micropost_path(@comment.micropost_id)
    else
      flash[:danger] = I18n.t('messages.failed.comment.created')
      redirect_to request.referrer || root_path
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @comment.update_attributes(comment_params)
        format.html { redirect_to micropost_path(@comment.micropost_id),
          flash: { success: I18n.t('messages.success.comment.update') } }
        format.js
      else
        format.html { redirect_to micropost_path(@comment.micropost_id),
          flash: { danger: I18n.t('messages.failed.comment.update') } }
        format.js
      end
    end

  end

  def destroy
    if @comment.destroy
      flash[:success] = I18n.t('messages.success.comment.deleted')
    else
      flash[:failed] = I18n.t('messages.success.comment.deleted')
    end
    redirect_to request.referrer || root_path
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :micropost_id)
  end

  def load_comment
    return if @comment = Comment.find_by(id: params[:id])
    flash[:danger] = I18n.t('messages.failed.comment.not_find')
    redirect_to root_path
  end

  def comment_owner
    return if @comment = current_user.comments.find_by(id: params[:id])

    flash[:danger] = I18n.t('messages.failed.comment.not_owner')
    redirect_to request.referrer || root_path
  end
end
