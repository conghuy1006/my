class RelationshipsController < ApplicationController
  before_action :check_login

  def create
    return unless @user = User.find_by(id: params[:followed_id])

    current_user.follow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    return unless @user = Relationship.find_by(id: params[:id]).followed

    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
