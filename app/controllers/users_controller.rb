class UsersController < ApplicationController
  before_action :logged_in_user,                only: :new
  before_action :check_login,                   except: [:new, :create, :show]
  before_action :load_user,                     except: [:index, :new, :create]
  before_action :admin_user, :not_current_user, only: :destroy
  before_action :correct_user,                  only: [:edit, :update]

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = I18n.t('messages.success.user.activate')
      redirect_to root_path
    else
      flash.now[:danger] = I18n.t('messages.failed.user.create')
      render :new
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = I18n.t('messages.success.user.update')
      redirect_to @user
    else
      flash.now[:danger] = I18n.t('messages.failed.user.update')
      render :edit
    end
  end

  def destroy
    if @user.destroy
      flash[:success] = I18n.t('messages.success.user.delete')
      redirect_to users_path
    else
      flash[:danger] = I18n.t('messages.failed.user.delete')
      redirect_to users_path
    end
  end

  def following
    @title = I18n.t('users.follow.following')
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = I18n.t('users.follow.followers')
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def correct_user
    return if current_user?(@user)

    flash[:danger] = I18n.t('messages.failed.user.correct_user')
    redirect_to root_path
  end

  def admin_user
    return if current_user.admin?

    flash[:danger] = I18n.t('messages.failed.user.permit')
    redirect_to root_path
  end

  def load_user
    return if @user = User.find_by(id: params[:id])

    flash[:danger] = I18n.t('messages.failed.user.find')
    redirect_to root_path
  end

  def not_current_user
    return unless current_user?(@user)

    flash[:danger] = I18n.t('messages.failed.user.delete_current_user')
    redirect_to users_path
  end
end
