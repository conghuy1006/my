class PasswordResetsController < ApplicationController
  before_action :load_user,         only: [:edit, :update]
  before_action :valid_token_reset, only: [:edit, :update]
  before_action :check_expiration,  only: [:edit, :update]
  before_action :logged_in_user,    only: :new

  def new
  end

  def create
    if @user = User.find_by(email: params[:password_reset][:email].downcase)
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = I18n.t('messages.success.password_reset.mail_sent')
      redirect_to root_path
    else
      flash.now[:danger] = I18n.t('messages.failed.password_reset.mail_sent')
      render :new
    end
  end

  def edit
  end

  def update
    if params[:user][:password].blank?
      @user.errors.add(:password, I18n.t('messages.failed.password_reset.not_empty'))
    elsif @user.update_attributes(user_params)
      log_in(@user)
      flash[:success] = I18n.t('messages.success.password_reset.has_been_reset')
      redirect_to @user and return
    end
    render :edit
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def load_user
    return if @user = User.find_by(email: params[:email], activated: true)

    flash[:danger] = I18n.t('messages.failed.password_reset.load_user')
    redirect_to root_path
  end

  def valid_token_reset
    return if @user.authenticated?(:reset, params[:id])

    flash[:danger]= I18n.t('messages.failed.password_reset.invalid_token')
    redirect_to root_path
  end

  def check_expiration
    return unless @user.password_reset_expired?

    flash[:danger] = I18n.t('messages.failed.password_reset.expired_reset_token')
    redirect_to new_password_reset_path
  end
end
