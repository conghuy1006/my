class SessionsController < ApplicationController
  before_action :logged_in_user, only: :new

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in(user)
        params[:session][:remember_me].to_i.zero? ? forget(user) : remember(user)
        redirect_back_or user
      else
        flash[:warning] = I18n.t('messages.failed.session.not_activate')
        redirect_to root_path
      end
    else
      flash.now[:danger] = I18n.t('messages.failed.session.login')
      render :new
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_path
  end
end
