module ApplicationHelper

  # Returns the full title on a per-page basis.        # Documentation comment
  def full_title(page_title = '')                      # Method def, optinal arg
    base_title = I18n.t('static_pages.title')          # Varriable assignment
    if page_title.empty?                               # Boolean test
      base_title                                       # Implicit return
    else
      "#{page_title} | #{ base_title}"                 # String concatenation
    end
  end
end
