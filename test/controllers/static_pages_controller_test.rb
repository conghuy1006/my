require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = I18n.t('static_pages.title')
  end

  test 'should get home' do
    get root_path
    assert_response :success
    assert_select 'title', "#{I18n.t('static_pages.home.home')} | #{@base_title}"
  end

  test 'should get help' do
    get help_path
    assert_response :success
    assert_select 'title', "#{I18n.t('static_pages.help.help')} | #{@base_title}"
  end

  test 'should get about' do
    get about_path
    assert_response :success
    assert_select 'title', "#{I18n.t('static_pages.about.about')} | #{@base_title}"
  end

  test 'should get contact' do
    get contact_path
    assert_response :success
    assert_select 'title', "#{I18n.t('static_pages.contact.contact')} | #{@base_title}"
  end
end
